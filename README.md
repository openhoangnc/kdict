Từ điển cho Kindle
------------------
Để tiện download trực tiếp từ browser trên Kindle, tôi tổng hợp lại các file từ điển và upload tại [Release](https://github.com/openhoangnc/kindle-dict/releases).

Nguồn từ điển sưu tập từ các trang trên Internet, tôi không biết đâu là trang gốc nên không dẫn link.
Nếu bạn là tác giả của bộ từ điển, xin hãy cho tôi biết để cập nhật thông tin, hoặc nếu có yêu cầu, tôi sẵn sàng gỡ bỏ khỏi trang này.

Link rút gọn đến trang download: [bit.do/kdict](http://bit.do/kdict)


Nguồn từ điển
------
http://www.mediafire.com/?wkai7p5hs9e2m

http://www.studyjapanese.net/2011/04/tu-ien-babylon-voi-glossaries-nhat-viet.html

Sẽ dùng để tạo một số file từ điển mới
